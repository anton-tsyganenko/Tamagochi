package com.example.anton.tamagochi;

/*
Copyright 2016 Anton Tsyganenko

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

public class Creature {
    int satiety; // сытость
    int vivacity; // бодрость
    int happiness; // счастье

    String name;

    public enum State {
        NORMAL, SLEEPING, PLAYING
    }

    State state;

    Creature(String name){
        this.name = name;

        satiety=100;
        vivacity=100;
        happiness=100;
        state=State.NORMAL;
    }

    void give (Item item) {
        satiety+= item.d_sat;
        vivacity+= item.d_viv;
        happiness+= item.d_hap;
    }

    void recalc(){
        if (state == State.NORMAL){
            satiety-=2;
            vivacity-=2;
            happiness-=2;

        }
        else if (state == State.PLAYING){
            satiety-=2;
            vivacity-=5;
            happiness+=7;
        }
        else if (state == State.SLEEPING){
            satiety-=1;
            vivacity+=3;
            happiness+=1;
        }
    }

    boolean isAlive(){
        return satiety > 0 && vivacity > 0 && happiness > 0;
    }

}
