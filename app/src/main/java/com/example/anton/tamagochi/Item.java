package com.example.anton.tamagochi;

/*
Copyright 2016 Anton Tsyganenko

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

public class Item {
    int d_sat;
    int d_viv;
    int d_hap;

    String name;

    int price;

    //TODO: picture

    public String toString() {
        return this.name + ": $"+ this.price;
    }

    Item (String name, int d_satiety, int d_vivacity, int d_happyness, int price){
        this.name = name;
        d_sat = d_satiety;
        d_viv = d_vivacity;
        d_hap = d_happyness;
        this.price = price;
    }
}
