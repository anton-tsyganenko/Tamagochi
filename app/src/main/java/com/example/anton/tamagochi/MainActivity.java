package com.example.anton.tamagochi;

/*
Copyright 2016 Anton Tsyganenko

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    static Player player;
    static Creature bob;
    DrawCanvas mycanvas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        player = new Player();
        bob = new Creature("Bob");

        writeStatus();
        Button stopButton = (Button)findViewById(R.id.button_stop);
        stopButton.setEnabled(false);

        final android.os.Handler customHandler = new android.os.Handler();
        final Runnable updateTimerThread = new Runnable()
        {
            public void run()
            {

                bob.recalc();
                player.money += 30; // just a random salary

                if(bob.isAlive()) {
                    writeStatus();
                    customHandler.postDelayed(this, 3000);
                } else {
                    TextView status=(TextView)findViewById(R.id.status_str);
                    status.setText("DEAD");
                    mycanvas.stop();

                }

            }
        };


        customHandler.postDelayed(updateTimerThread, 0);

        mycanvas = (DrawCanvas)findViewById(R.id.SurfaceView01);

    }

    @Override
    protected void onResume(){
        super.onResume();
        mycanvas.resume();
    }


    public void sleep_btn_handler(View view){
        bob.state=Creature.State.SLEEPING;
        Button sleepButton = (Button)findViewById(R.id.button_sleep);
        Button playButton = (Button)findViewById(R.id.button_play);
        Button stopButton = (Button)findViewById(R.id.button_stop);
        sleepButton.setEnabled(false);
        playButton.setEnabled(true);
        stopButton.setEnabled(true);
    }

    public void play_btn_handler(View view) {
        bob.state=Creature.State.PLAYING;
        Button sleepButton = (Button)findViewById(R.id.button_sleep);
        Button playButton = (Button)findViewById(R.id.button_play);
        Button stopButton = (Button)findViewById(R.id.button_stop);
        sleepButton.setEnabled(true);
        playButton.setEnabled(false);
        stopButton.setEnabled(true);
    }

    public void norm_btn_handler(View view){
        bob.state=Creature.State.NORMAL;
        Button sleepButton = (Button)findViewById(R.id.button_sleep);
        Button playButton = (Button)findViewById(R.id.button_play);
        Button stopButton = (Button)findViewById(R.id.button_stop);
        sleepButton.setEnabled(true);
        playButton.setEnabled(true);
        stopButton.setEnabled(false);
    }

    public void give_btn_handler(View view){
        mycanvas.stop();
        Intent i = new Intent(this, Market.class);
        startActivity(i);
    }


    void writeStatus(){
        TextView status=(TextView)findViewById(R.id.status_str);
        status.setText("Satiety: " + bob.satiety + ", Vivacity: " + bob.vivacity + ", Happiness: " + bob.happiness + ", Money: " + player.money);
    }

}
