package com.example.anton.tamagochi;

/*
Copyright 2016 Anton Tsyganenko

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

/**
 * Created by anton on 4/23/16.
 */
public class DrawCanvas extends SurfaceView implements SurfaceHolder.Callback {
    private CanvasThread canvasThread;

    public DrawCanvas(Context context) {
        super(context);
    }

    public DrawCanvas(Context context, AttributeSet attrs) {
        super(context, attrs);

        this.getHolder().addCallback(this);
        this.canvasThread = new CanvasThread(getHolder());
        this.setFocusable(true);
    }

    public DrawCanvas(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void startDrawImage() {
        canvasThread.setRunning(true);
        try{
            canvasThread.start();
        } catch (Exception a){}
    }

    public void stop(){
        canvasThread.setRunning(false);
    }

    public void resume(){
        canvasThread = new CanvasThread(getHolder());
    }

    @Override
    public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {

    }

    @Override
    public void surfaceCreated(SurfaceHolder arg0) {
        startDrawImage();

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder arg0) {
        boolean retry = true;
        canvasThread.setRunning(false);
        while(retry) {
            try {
                canvasThread.join();
                retry = false;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
    }

    public static int multStrip(int x){
        if (x > 200) return 255;
        else return (x * 255 / 200);
    }


    private class CanvasThread extends Thread {
        private SurfaceHolder surfaceHolder;
        public boolean isRun = true;

        public CanvasThread(SurfaceHolder holder) {
            this.surfaceHolder = holder;
        }

        public void setRunning(boolean run) {
            this.isRun = run;
        }

        @Override
        public void run() {
            Canvas c;
            int x;
            int y;
            int r;
            int minc;
            Paint mPaint = new Paint();

            while(isRun) {
                    c = null;
                    try {
                        c = this.surfaceHolder.lockCanvas(null);
                        synchronized (this.surfaceHolder) {
                            if (c!=null && MainActivity.bob.isAlive()) {
                                x = c.getWidth() / 2;
                                y = (int) ((float) c.getHeight() / 2.5);
                                minc = Math.min(x, y);
                                r = Math.min(Math.min(MainActivity.bob.satiety, MainActivity.bob.vivacity), MainActivity.bob.happiness) * (int)((float)minc * 0.005);
                                if (r > (minc * 3 / 5)) r = minc * 3 / 5;
                                r += minc*0.1;
                                c.drawColor(Color.WHITE);

                                mPaint.setStyle(Paint.Style.FILL);
                                mPaint.setColor(Color.rgb(multStrip(MainActivity.bob.satiety), multStrip(MainActivity.bob.vivacity), multStrip(MainActivity.bob.happiness)));
                                c.drawCircle(x, y, r, mPaint);
                                mPaint.setStyle(Paint.Style.STROKE);
                                mPaint.setStrokeWidth(r / 10);
                                mPaint.setColor(Color.BLACK);
                                c.drawCircle(x, y, r, mPaint);
                                surfaceHolder.unlockCanvasAndPost(c);
                            }
                        }
                    } finally {

                }
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        }
    }
}
