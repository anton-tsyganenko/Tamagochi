package com.example.anton.tamagochi;

/*
Copyright 2016 Anton Tsyganenko

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;

public class Market extends ListActivity {

    ArrayList<Item> lst;
    ArrayAdapter<Item> adapter;

    Item[] items;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        items = new Item[]{
                new Item("Hamburger", 30, 0, 20, 100),
                new Item("Salad", 20, 0, 5, 70),
                new Item("Tea", 5, 5, 5, 20),
                new Item("Red Bull", 0, 50, 0, 50),
                new Item("Quadcopter", 0, -5, 50, 3000),
                new Item("Gun", 0, 0, 30, 1000),
                new Item("Dead cat", 0, 0, 0, 0)
        };

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_market);
        lst = new ArrayList<Item>(Arrays.asList(items));
        adapter = new ArrayAdapter<Item>(this, android.R.layout.simple_list_item_1,lst);
        setListAdapter(adapter);
    }
    @Override
    protected void onListItemClick(ListView l, View v, int position,long id){
        final Item cur = (Item)getListAdapter().getItem(position);

        AlertDialog.Builder builder = new AlertDialog.Builder(Market.this);
        final TextView text = new TextView(this);
        text.setText("Happyness: "+cur.d_hap+", Vivacity: "+cur.d_viv+", Satiety: "+cur.d_sat);
        builder.setView(text);
        builder.setTitle(cur.name);
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.setPositiveButton("Buy", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                    if (MainActivity.player.money < cur.price) Toast.makeText(Market.this, "You don't have enough money", Toast.LENGTH_SHORT).show();
                    else if (!MainActivity.bob.isAlive()) Toast.makeText(Market.this, "The creature is dead", Toast.LENGTH_SHORT).show();
                    else if (MainActivity.bob.state == Creature.State.SLEEPING) Toast.makeText(Market.this, "I am speeling", Toast.LENGTH_SHORT).show();
                    else {
                        MainActivity.bob.give(cur);
                        MainActivity.player.money -= cur.price;
                    }

            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }
}
